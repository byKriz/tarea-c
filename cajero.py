class Cajero:

    _USERDATA = 'userdata.txt'

    @classmethod
    def makeUser(cls, user, cash):
        try:
            with open(cls._USERDATA, 'w') as f:
                return f.write(f'{user}\n{cash}')
        except FileNotFoundError:
            return False


    @classmethod
    def fileExists(cls):
        try:
            with open(cls._USERDATA, 'r') as f:
                return True
        except FileNotFoundError:
            print('\nRegistro de nuevo usuario')
            username = input('Ingrese su nombre de usuario: ')
            cash = input('Ingrese su monto a depositar: ')
            cls.makeUser(username, cash)


    @classmethod
    def withdrawCash(cls, cash):
        balance = cls.getCash()
        tickets = {'100':0, '50':0, '20':0, '10':0, '5':0, '2':0, '1':0}
        if balance >= cash:
            balance -= cash
            cls.setCash(balance)
            for key, value in tickets.items():
                while cash >= int(key):
                    tickets[key] += 1
                    cash -= int(key)
            for key, value in tickets.items():
                if value > 0:
                    if value == 1:
                        print(f'Se ha retirado un billete de {key}$')
                    else:
                        print(f'Se ha retirado {value} billetes de {key}$')
        else:
            print('No tiene saldo suficiente para realizar ese retiro')


    @classmethod
    def getUser(cls):
        try:
            with open(cls._USERDATA, 'r', encoding='utf-8') as f:
                return f.read().split('\n')[0]
        except FileNotFoundError:
            return False


    @classmethod
    def getCash(cls):
        try:
            with open(cls._USERDATA, 'r', encoding='utf-8') as f:
                return int(f.read().split('\n')[1])
        except FileNotFoundError:
            return False


    @classmethod
    def setCash(cls, cash):
        username = cls.getUser()
        try:
            with open(cls._USERDATA, 'w', encoding='utf-8') as f:
                return f.write(f'{username}\n{cash}')
        except FileNotFoundError:
            return False

    
    @classmethod
    def getUserBalance(cls):
        username = cls.getUser()
        balance = cls.getCash()
        return f'\nUsuario: {username} \nBalance: {balance}'
    


    
