from cajero import Cajero

menu = '''
Elige alguna de las siguientes opciones:
1. Consultar saldo
2. Retirar fondos
3. Salir'''

if __name__ == '__main__':
    Cajero.fileExists()
    print(' Bienvenido a Banco Panamá '.center(50, '*'))
    print(menu)

    option = None
    while True:
        option = input('Ingrese una opcion: ')
        if option == '1':
            print(Cajero.getUserBalance())
            print()
        elif option == '2':
            withdraw = int(input('Ingrese el monto a retirar (nª enteros): '))
            Cajero.withdrawCash(withdraw)
        elif option == '3':
            print('Gracias por usar el banco')
            break
        else:
            print('Ingrese una opcion valida')
